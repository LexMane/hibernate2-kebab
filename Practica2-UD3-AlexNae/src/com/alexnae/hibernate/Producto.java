package com.alexnae.hibernate;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
public class Producto {
    private int idProducto;
    private String nombreProducto;
    private int salsa;
    private double precio;
    private List<Pedido> pedidos;

    public Producto(int idProducto, String nombreProducto, int salsa, double precio) {
        this.idProducto = idProducto;
        this.nombreProducto = nombreProducto;
        this.salsa = salsa;
        this.precio = precio;
    }

    public Producto() {

    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_producto")
    public int getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }

    @Basic
    @Column(name = "nombre_producto")
    public String getNombreProducto() {
        return nombreProducto;
    }

    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    @Basic
    @Column(name = "salsa")
    public int getSalsa() {
        return salsa;
    }

    public void setSalsa(int salsa) {
        this.salsa = salsa;
    }

    @Basic
    @Column(name = "precio")
    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Producto producto = (Producto) o;
        return idProducto == producto.idProducto &&
                salsa == producto.salsa &&
                Double.compare(producto.precio, precio) == 0 &&
                Objects.equals(nombreProducto, producto.nombreProducto);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idProducto, nombreProducto, salsa, precio);
    }

    @ManyToMany(mappedBy = "productos", cascade = CascadeType.DETACH)
    public List<Pedido> getPedidos() {
        return pedidos;
    }

    public void setPedidos(List<Pedido> pedidos) {
        this.pedidos = pedidos;
    }

    @Override
    public String toString() {
        return "Producto{" +
                "idProducto=" + idProducto +
                ", nombreProducto='" + nombreProducto + '\'' +
                ", salsa=" + salsa +
                ", precio=" + precio +
                '}';
    }
}
