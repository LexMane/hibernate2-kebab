package com.alexnae.hibernate;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Entity
public class Empleado {
    private int idEmpleado;
    private String nombreEmpleado;
    private int salario;
    private Date fechaNacimiento;
    private Local local;
    private List<Pedido> pedidos;

    public Empleado(int idEmpleado, String nombreEmpleado, int salario, Date fechaNacimiento, Local local) {
        this.idEmpleado = idEmpleado;
        this.nombreEmpleado = nombreEmpleado;
        this.salario = salario;
        this.fechaNacimiento = fechaNacimiento;
        this.local = local;
    }

    public Empleado() {

    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_empleado")
    public int getIdEmpleado() {
        return idEmpleado;
    }

    public void setIdEmpleado(int idEmpleado) {
        this.idEmpleado = idEmpleado;
    }

    @Basic
    @Column(name = "nombre_empleado")
    public String getNombreEmpleado() {
        return nombreEmpleado;
    }

    public void setNombreEmpleado(String nombreEmpleado) {
        this.nombreEmpleado = nombreEmpleado;
    }

    @Basic
    @Column(name = "salario")
    public int getSalario() {
        return salario;
    }

    public void setSalario(int salario) {
        this.salario = salario;
    }

    @Basic
    @Column(name = "fecha_nacimiento")
    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Empleado empleado = (Empleado) o;
        return idEmpleado == empleado.idEmpleado &&
                salario == empleado.salario &&
                Objects.equals(nombreEmpleado, empleado.nombreEmpleado) &&
                Objects.equals(fechaNacimiento, empleado.fechaNacimiento);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idEmpleado, nombreEmpleado, salario, fechaNacimiento);
    }

    @ManyToOne
    @JoinColumn(name = "id_local", referencedColumnName = "id_local", nullable = false)
    public Local getLocal() {
        return local;
    }

    public void setLocal(Local local) {
        this.local = local;
    }

    @OneToMany(mappedBy = "empleado", cascade = CascadeType.ALL)
    public List<Pedido> getPedidos() {
        return pedidos;
    }

    public void setPedidos(List<Pedido> pedidos) {
        this.pedidos = pedidos;
    }

    @Override
    public String toString() {
        return "Empleado{" +
                "idEmpleado=" + idEmpleado +
                ", nombreEmpleado='" + nombreEmpleado + '\'' +
                ", salario=" + salario +
                ", fechaNacimiento=" + fechaNacimiento +
                ", local=" + local.getIdLocal() +
                '}';
    }
}
