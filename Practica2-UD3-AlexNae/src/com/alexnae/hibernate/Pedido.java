package com.alexnae.hibernate;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "pedidos", schema = "hiberkebab")
public class Pedido {
    private int idPedido;
    private Date fechaPedido;
    private boolean domicilio;
    private boolean pagado;
    private Empleado empleado;
    private List<Producto> productos;

    public Pedido(int idPedido, Date fechaPedido, boolean domicilio, boolean pagado) {
        this.idPedido = idPedido;
        this.fechaPedido = fechaPedido;
        this.domicilio = domicilio;
        this.pagado = pagado;
    }

    public Pedido() {

    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_pedido")
    public int getIdPedido() {
        return idPedido;
    }

    public void setIdPedido(int idPedido) {
        this.idPedido = idPedido;
    }

    @Basic
    @Column(name = "fecha_pedido")
    public Date getFechaPedido() {
        return fechaPedido;
    }

    public void setFechaPedido(Date fechaPedido) {
        this.fechaPedido = fechaPedido;
    }

    @Basic
    @Column(name = "domicilio")
    public boolean getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(boolean domicilio) {
        this.domicilio = domicilio;
    }

    @Basic
    @Column(name = "pagado")
    public boolean getPagado() {
        return pagado;
    }

    public void setPagado(boolean pagado) {
        this.pagado = pagado;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pedido pedido = (Pedido) o;
        return idPedido == pedido.idPedido &&
                domicilio == pedido.domicilio &&
                pagado == pedido.pagado &&
                Objects.equals(fechaPedido, pedido.fechaPedido);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idPedido, fechaPedido, domicilio, pagado);
    }

    @ManyToOne
    @JoinColumn(name = "id_empleado", referencedColumnName = "id_empleado", nullable = false)
    public Empleado getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }

    @ManyToMany(cascade = CascadeType.DETACH)
    @JoinTable(name = "pedidos_producto", catalog = "", schema = "hiberkebab", joinColumns = @JoinColumn(name = "id_producto", referencedColumnName = "id_pedido", nullable = false), inverseJoinColumns = @JoinColumn(name = "id_pedido", referencedColumnName = "id_producto", nullable = false))
    public List<Producto> getProductos() {
        return productos;
    }

    public void setProductos(List<Producto> productos) {
        this.productos = productos;
    }

    @Override
    public String toString() {
        return "Pedido{" +
                "idPedido=" + idPedido +
                ", fechaPedido=" + fechaPedido +
                ", domicilio=" + domicilio +
                ", pagado=" + pagado +
                ", empleado=" + empleado +
                '}';
    }
}
