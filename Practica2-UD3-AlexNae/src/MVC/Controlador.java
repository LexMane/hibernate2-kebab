package MVC;

import com.alexnae.hibernate.*;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.sql.Date;
import java.util.ArrayList;

/**
 * CLASE CONTROLADOR donde interactuaremos con la interfaz del usuario, es decir, el nexo entre Vista y Modelo
 */
public class Controlador implements ActionListener, ListSelectionListener, WindowListener {
    private Vista vista;
    private Modelo modelo;

    /**
     * Constructor de clase con Vista y Modelo mencionados
     * @param vista nuestra vista principal
     * @param modelo nuesto modelo
     */
    public Controlador(Vista vista, Modelo modelo) {
        this.vista = vista;
        this.modelo = modelo;

        addActionListeners(this);
        addListSelectionListener(this);
    }

    /**
     * Nos permitirá reaccionar a los distintos botones de Alta, Modificación, Eliminación, acceso a menús...
     * Y posteriormente conectará al modelo que a su vez conectará con la base de datos (Todo automatizado)
     * @param e el evento seleccionado (Un botón por ejemplo)
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();

        switch (comando) {
            case "Salir":
                modelo.desconectar();
                System.exit(0);
                break;
            case "Conectar":
                modelo.conectar();
                vista.conexionItem.setEnabled(false);
                listarTodo();
                break;
            case "Actualizar":
                if (!vista.conexionItem.isEnabled()){
                    listarTodo();
                }
                break;
            case "btnRegistrarLocal":
                Local nuevoLocal = new Local();
                nuevoLocal.setNombreLocal(vista.txtNombreLocal.getText());
                nuevoLocal.setCiudad(vista.txtCiudad.getText());
                nuevoLocal.setCodigoPostal(Integer.parseInt(vista.txtCodPostal.getText()));
                modelo.altaLocal(nuevoLocal);
                limpiarCamposLocal();
                listarTodo();
                break;
            case "btnModificarLocal":
                Local localModificado = (Local) vista.listaLocal.getSelectedValue();
                localModificado.setNombreLocal(vista.txtNombreLocal.getText());
                localModificado.setCiudad(vista.txtCiudad.getText());
                localModificado.setCodigoPostal(Integer.parseInt(vista.txtCodPostal.getText()));
                modelo.modificarLocal(localModificado);
                listarTodo();
                break;

            case "btnEliminarLocal":
                Local localEliminado = (Local) vista.listaLocal.getSelectedValue();
                modelo.borrarLocal(localEliminado);
                limpiarCamposLocal();
                listarTodo();
                break;
            case "btnRegistrarEmpleado":
                Empleado empleadoInsertado = new Empleado();
                empleadoInsertado.setNombreEmpleado(vista.txtNombreEmpleado.getText());
                empleadoInsertado.setSalario(Integer.parseInt(vista.txtSalario.getText()));
                empleadoInsertado.setFechaNacimiento(Date.valueOf(vista.dpFechaNacimiento.getDateStringOrEmptyString()));
                empleadoInsertado.setLocal((Local) vista.cbLocal.getItemAt(vista.cbLocal.getSelectedIndex()));
                modelo.altaEmpleado(empleadoInsertado);
                limpiarCamposEmpleado();
                listarTodo();
                break;
            case "btnModificarEmpleado":
                Empleado empleadoModificado = (Empleado) vista.listaEmpleados.getSelectedValue();
                empleadoModificado.setNombreEmpleado(vista.txtNombreEmpleado.getText());
                empleadoModificado.setSalario(Integer.parseInt(vista.txtSalario.getText()));
                empleadoModificado.setFechaNacimiento(Date.valueOf(vista.dpFechaNacimiento.getDateStringOrEmptyString()));
                modelo.modificarEmpleado(empleadoModificado);
                listarTodo();
                break;
            case "btnEliminarEmpleado":
                Empleado empleadoEliminado = (Empleado) vista.listaEmpleados.getSelectedValue();
                modelo.borrarEmpleado(empleadoEliminado);
                limpiarCamposEmpleado();
                listarTodo();
                break;
            case "btnVerEmpleadoLocal":
               // System.out.println("entro");
                Local localNuevo = (Local) vista.listaLocal.getSelectedValue();
                localNuevo.setEmpleados(modelo.getEmpleadoLocal(localNuevo));
                listarEmpleadoLocal(modelo.getEmpleadoLocal(localNuevo));
                break;
            case "btnRegistrarPedido":
                Pedido pedidoNuevo = new Pedido();
                pedidoNuevo.setDomicilio(vista.cbDomicilio.isSelected());
                pedidoNuevo.setPagado(vista.cbPagado.isSelected());
                pedidoNuevo.setFechaPedido((Date.valueOf(vista.dpPedidos.getDate())));
                pedidoNuevo.setEmpleado((Empleado) vista.cbEmpleado.getSelectedItem());
                modelo.altaPedido(pedidoNuevo);
                limpiarCamposPedido();
                listarTodo();
                break;
            case "btnModificarPedido":
                Pedido pedidoModificado = (Pedido)vista.listaPedido.getSelectedValue();
                pedidoModificado.setDomicilio(vista.cbDomicilio.isSelected());
                pedidoModificado.setPagado(vista.cbPagado.isSelected());
                pedidoModificado.setFechaPedido((Date.valueOf(vista.dpPedidos.getDate())));
                pedidoModificado.setEmpleado((Empleado) vista.cbEmpleado.getSelectedItem());
                modelo.modificarPedido(pedidoModificado);
                listarTodo();
                break;
            case "btnEliminarPedido":
                Pedido pedidoBorrado = (Pedido) vista.listaPedido.getSelectedValue();
                modelo.borrarPedido(pedidoBorrado);
                limpiarCamposPedido();
                listarTodo();
                break;

            case "btnRegistrarProducto":
                Producto productoNuevo = new Producto();
                productoNuevo.setNombreProducto(vista.txtNombreProducto.getText());
                productoNuevo.setPrecio(Double.parseDouble(vista.txtPrecio.getText()));
                productoNuevo.setSalsa(Integer.parseInt(vista.txtNumSalsas.getText()));
                modelo.altaProducto(productoNuevo);
                limpiarCamposProducto();
                listarTodo();
                break;
            case "btnModificarProducto":
                Producto productoModificado = (Producto) vista.listaProducto.getSelectedValue();
                productoModificado.setNombreProducto(vista.txtNombreProducto.getText());
                productoModificado.setPrecio(Double.parseDouble(vista.txtPrecio.getText()));
                productoModificado.setSalsa(Integer.parseInt(vista.txtNumSalsas.getText()));
                modelo.modificarProducto(productoModificado);
                listarTodo();
                break;
            case "btnEliminarProducto":
                Producto productoEliminado = (Producto) vista.listaProducto.getSelectedValue();
                modelo.borrarProducto(productoEliminado);
                limpiarCamposProducto();
                listarTodo();
                break;

        }
    }

    /**
     * Lista todas las tablas de la Vista sacando los datos del modelo
     */
    private void listarTodo() {
        listarLocales(modelo.getLocal());
        listarEmpleados(modelo.getEmpleado());
        listarPedidos(modelo.getPedido());
        listarProductos(modelo.getProducto());
        listarLocalCb(modelo.getLocal());
        listarEmpleadosCb(modelo.getEmpleado());
    }

    /**
     * Método sobreescrito que nos permite detectar si hacemos click en una de las listas para auto-rellenar los campos
     * y así tenerlo en los TextFields para sobreescribirlos/modificarlos/borrarlos
     * @param e el evento de lista
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting()) {
            if (e.getSource() == vista.listaLocal) {
                Local seleccionLocal = (Local) vista.listaLocal.getSelectedValue();
                vista.txtNombreLocal.setText(seleccionLocal.getNombreLocal());
                vista.txtCiudad.setText(seleccionLocal.getCiudad());
                vista.txtCodPostal.setText(String.valueOf(seleccionLocal.getCodigoPostal()));

//                if (seleccionLocal.getInstructor() != null) {
//                    vista.comboSociosInstructor.setSelectedItem(seleccionLocal.getInstructor());
//                } else {
//                    vista.txtSociosInstructor.setText("");
//                }

            } else if (e.getSource() == vista.listaEmpleados) {
                Empleado seleccionEmpleado = (Empleado) vista.listaEmpleados.getSelectedValue();
                vista.txtNombreEmpleado.setText(seleccionEmpleado.getNombreEmpleado());
                vista.txtSalario.setText(String.valueOf(seleccionEmpleado.getSalario()));
                vista.dpFechaNacimiento.setDate(seleccionEmpleado.getFechaNacimiento().toLocalDate());
                vista.cbLocal.setSelectedItem(seleccionEmpleado.getLocal());
                listarLocalCb(modelo.getLocal());

            } else if (e.getSource() == vista.listaPedido){
                Pedido seleccionPedido = (Pedido) vista.listaPedido.getSelectedValue();
                vista.cbPagado.setSelected(seleccionPedido.getPagado());
                vista.cbDomicilio.setSelected(seleccionPedido.getDomicilio());
                vista.dpPedidos.setDate(seleccionPedido.getFechaPedido().toLocalDate());
                vista.cbEmpleado.setSelectedItem(seleccionPedido.getEmpleado());

            } else if (e.getSource() == vista.listaProducto){
                Producto seleccionProducto = (Producto) vista.listaProducto.getSelectedValue();
                vista.txtNombreProducto.setText(seleccionProducto.getNombreProducto());
                vista.txtNumSalsas.setText(String.valueOf(seleccionProducto.getSalsa()));
                vista.txtPrecio.setText(String.valueOf(seleccionProducto.getPrecio()));
        }
        }
    }


    /**
     * Método que añade todos los listeners de la Vista
     * @param listener el listener que añadir (En este caso la clase hereda de Listener así que será this)
     */
    private void addActionListeners(ActionListener listener) {
        vista.conexionItem.addActionListener(listener);
        vista.salirItem.addActionListener(listener);
        vista.actualizarItem.addActionListener(listener);

        vista.btnEliminarEmpleado.addActionListener(listener);
        vista.btnEliminarLocal.addActionListener(listener);
        vista.btnEliminarPedido.addActionListener(listener);
        vista.btnEliminarProducto.addActionListener(listener);

        vista.btnModificarEmpleado.addActionListener(listener);
        vista.btnModificarLocal.addActionListener(listener);
        vista.btnModificarPedido.addActionListener(listener);
        vista.btnModificarProducto.addActionListener(listener);

        vista.btnRegistrarEmpleado.addActionListener(listener);
        vista.btnRegistrarLocal.addActionListener(listener);
        vista.btnRegistrarPedido.addActionListener(listener);
        vista.btnRegistrarProducto.addActionListener(listener);
        vista.btnVerEmpleadoLocal.addActionListener(listener);

    }

    /**
     * Método que permite registrar los cambios en las tablas mediante el conocido ListSelectionListener
     * @param listener el listener de la tabla a mirar (En nuestro caso this)
     */
    private void addListSelectionListener(ListSelectionListener listener) {
        vista.listaLocal.addListSelectionListener(listener);
        vista.listaEmpleados.addListSelectionListener(listener);
        vista.listaProducto.addListSelectionListener(listener);
        vista.listaPedido.addListSelectionListener(listener);
    }


    /**
     * Limpia todos los campos de SOCIOS
     */
    private void limpiarCamposLocal() {
        vista.txtNombreLocal.setText("");
        vista.txtCiudad.setText("");
        vista.txtCodPostal.setText("");
    }

    /**
     * Limpia todos los campos de INSTRUCTORES
     */
    private void limpiarCamposEmpleado() {
        vista.txtNombreEmpleado.setText("");
        vista.txtSalario.setText("");
        vista.dpFechaNacimiento.setText("");
    }

    /**
     * Limpia todos los campos de ACTIVIDADES
     */
    private void limpiarCamposPedido() {

        vista.dpPedidos.clear();
    }

    /**
     * Limpia todos los campos de PROVEEDORES
     */
    private void limpiarCamposProducto() {
        vista.txtNombreProducto.setText("");
        vista.txtPrecio.setText("");
        vista.txtNumSalsas.setText("");
    }


    /**
     * Lista los locales de la base de datos en la pestaña de locales
     * @param locales lista de locales (sacada de modelo)
     */
    private void listarLocales(ArrayList<Local> locales) {
        vista.dlmLocal.clear();
        for(Local local : locales){
            vista.dlmLocal.addElement(local);
        }
    }

    /**
     * Lista los socios por instructor de la base de datos en la pestaña de instructor
     * @param localEmpleado lista de socios (sacada de modelo)
     */
    public void listarEmpleados (ArrayList<Empleado> localEmpleado) {
        vista.dlmEmpleado.clear();
        for(Empleado empleado : localEmpleado){
            vista.dlmEmpleado.addElement(empleado);
        }

    }

    /**
     * Lista los Instructores de la base de datos en la pestaña de Instructores
     * @param lista lista de Instructores (sacada de modelo)
     */
    public void listarEmpleadoLocal(ArrayList<Empleado> lista){
        vista.dlmEmpleadoLocal.clear();
        for(Empleado empleado : lista){
            vista.dlmEmpleadoLocal.addElement(empleado);
        }

//        vista.comboSociosInstructor.removeAllItems();
//        ArrayList<Instructores> ins = modelo.getEmpleado();
//
//        for (Instructores in : ins){
//            vista.comboSociosInstructor.addItem(in);
//        }
//        vista.comboSociosInstructor.setSelectedIndex(-1);

    }

    public void listarLocalCb(ArrayList<Local> lista){
        vista.dlmLocal.clear();
        for(Local local : lista){
            vista.dlmLocal.addElement(local);
        }

        vista.cbLocal.removeAllItems();
        ArrayList<Local> locales = modelo.getLocal();

      for (Local local : locales){
           vista.cbLocal.addItem(local);
        }
        vista.cbLocal.setSelectedIndex(-1);


    }
    public void listarEmpleadosCb(ArrayList<Empleado> lista){
        vista.dlmEmpleado.clear();
        for(Empleado empleado : lista){
            vista.dlmEmpleado.addElement(empleado);
        }

        vista.cbEmpleado.removeAllItems();
        ArrayList<Empleado> empleados = modelo.getEmpleado();

        for (Empleado empleado : empleados){
            vista.cbEmpleado.addItem(empleado);
        }
        vista.cbEmpleado.setSelectedIndex(-1);


    }

    /**
     * Lista los Actividades de la base de datos en la pestaña de Actividades
     * @param lista lista de Actividades (sacada de modelo)
     */
    public void listarPedidos(ArrayList<Pedido> lista){
        vista.dlmPedido.clear();
        for(Pedido pedido : lista){
            vista.dlmPedido.addElement(pedido);
        }

    }

    /**
     * Lista los Materiales de la base de datos en la pestaña de Materiales
     * @param lista lista de Materiales (sacada de modelo)
     */
    public void listarProductos(ArrayList<Producto> lista){
        vista.dlmProducto.clear();
        for(Producto producto : lista){
            vista.dlmProducto.addElement(producto);
        }
    }

    @Override
    public void windowOpened(WindowEvent e) {
        listarTodo();
    }

    @Override
    public void windowClosing(WindowEvent e) {

    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }
}
