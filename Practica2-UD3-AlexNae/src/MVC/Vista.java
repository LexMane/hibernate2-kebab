package MVC;
import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;
import java.awt.*;

/**
 * Clase Vista que hereda de JFrame que permite la utilización de la interfaz
 */
public class Vista extends JFrame {
    private JPanel panel1;
    private JFrame frame;

    private JTabbedPane tabbedPane1;
    JTextField txtNombreLocal;
    JTextField txtNombreEmpleado;
    JTextField txtSalario;
    DatePicker dpFechaNacimiento;
    DatePicker dpPedidos;
    JTextField txtNombreProducto;
    JTextField txtPrecio;

    JList listaLocal;
    JList listaEmpleados;
    JList listaProducto;
    JList listaPedido;
    JList listaEmpleadoLocal;

    JCheckBox cbDomicilio;
    JCheckBox cbPagado;
    JTextField txtCiudad;
    JTextField txtCodPostal;
    JButton btnModificarLocal;
    JButton btnEliminarLocal;
    JButton btnRegistrarLocal;
    JButton btnRegistrarEmpleado;
    JButton btnModificarEmpleado;
    JButton btnEliminarEmpleado;
    JButton btnRegistrarPedido;
    JButton btnModificarPedido;
    JButton btnEliminarPedido;
    JButton btnRegistrarProducto;
    JButton btnModificarProducto;
    JButton btnEliminarProducto;
    JComboBox cbLocal;
    JComboBox cbEmpleado;
    JTextField txtNumSalsas;
    JButton btnVerEmpleadoLocal;

    DefaultListModel dlmLocal;
    DefaultListModel dlmEmpleado;
    DefaultListModel dlmPedido;
    DefaultListModel dlmProducto;


    //Filtros
    DefaultListModel dlmEmpleadoLocal;

    JMenuItem conexionItem;
    JMenuItem salirItem;
    JMenuItem actualizarItem;


    /**
     * Constructor de clase vacío que inizializa lo necesario
     */
    public Vista(){
        frame = new JFrame("Vista");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        this.setSize(new Dimension(this.getWidth()+500, this.getHeight()+200));
        frame.setLocationRelativeTo(null);

        crearMenu();
        crearModelos();

    }

    /**
     * Métodoque permite crear los modelos, es decir añadir DLM a las listas
     */
    private void crearModelos() {
        dlmLocal = new DefaultListModel();
        listaLocal.setModel(dlmLocal);

        dlmEmpleado = new DefaultListModel();
        listaEmpleados.setModel(dlmEmpleado);

        dlmPedido = new DefaultListModel();
        listaPedido.setModel(dlmPedido);

        dlmProducto = new DefaultListModel();
        listaProducto.setModel(dlmProducto);

        dlmEmpleadoLocal = new DefaultListModel();
        listaEmpleadoLocal.setModel(dlmEmpleadoLocal);

    }


    /**
     * Método que permite crear un menú y añadir actionCommands
     */
    private void crearMenu() {
        JMenuBar barra = new JMenuBar();
        JMenu menu = new JMenu("Archivo");

        conexionItem = new JMenuItem("Conectar");
        conexionItem.setActionCommand("Conectar");

        actualizarItem = new JMenuItem("Actualizar");
        actualizarItem.setActionCommand("Actualizar");

        salirItem = new JMenuItem("Salir");
        salirItem.setActionCommand("Salir");

        menu.add(conexionItem);
        menu.add(actualizarItem);
        menu.add(salirItem);
        barra.add(menu);
        frame.setJMenuBar(barra);
    }
}
