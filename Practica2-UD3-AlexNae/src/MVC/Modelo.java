package MVC;

import com.alexnae.hibernate.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import javax.persistence.Query;
import java.util.ArrayList;

/**
 * Clase Modelo, que será la encargada de conectar la aplicación con hibernate, que a su vez conectará con la BBDD
 */
public class Modelo {
    SessionFactory sessionFactory;
    Session sesion;

    /**
     * Desconecta de hibernate
     */
    public void desconectar() {
        //Cierro la factoria de sessiones
        if(sessionFactory != null && sessionFactory.isOpen())
            sessionFactory.close();
    }

    /**
     * Conecta hibernate con las configuraciones oportuna (addAnnotatedClass)
     */
    public void conectar() {
        Configuration configuration = new Configuration();
        //Cargo el fichero Hibernate.cfg.xml
        configuration.configure("hibernate.cfg.xml");

        //Indico la clase mapeada con anotaciones
        configuration.addAnnotatedClass(Empleado.class);
        configuration.addAnnotatedClass(Local.class);
        configuration.addAnnotatedClass(Pedido.class);
        configuration.addAnnotatedClass(PedidosProducto.class);
        configuration.addAnnotatedClass(Producto.class);


        //Creamos un objeto ServiceRegistry a partir de los parámetros de configuración
        //Esta clase se usa para gestionar y proveer de acceso a servicios
        StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().applySettings(
                configuration.getProperties()).build();

        //finalmente creamos un objeto sessionfactory a partir de la configuracion y del registro de servicios
        sessionFactory = configuration.buildSessionFactory(ssr);
        sesion=sessionFactory.openSession();

    }

    public void altaLocal(Local nuevoLocal) {
        //Obtengo una session a partir de la factoria de sesiones

        sesion.beginTransaction();
        sesion.save(nuevoLocal);
        sesion.getTransaction().commit();

    }

    public ArrayList<Local> getLocal() {
        Query query = sesion.createQuery("FROM Local");
        ArrayList<Local> lista = (ArrayList<Local>)query.getResultList();
        return lista;
    }

    public void modificarLocal(Local localSeleccionado) {
        sesion.beginTransaction();
        sesion.saveOrUpdate(localSeleccionado);
        sesion.getTransaction().commit();
    }

    public void borrarLocal(Local localBorrado) {
        sesion.beginTransaction();
        sesion.delete(localBorrado);
        sesion.getTransaction().commit();
    }


    public void altaEmpleado(Empleado nuevoEmpleado) {
        sesion.beginTransaction();
        sesion.save(nuevoEmpleado);
        sesion.getTransaction().commit();


    }


    public ArrayList<Empleado> getEmpleado() {
        Query query = sesion.createQuery("FROM Empleado ");
        ArrayList<Empleado> lista = (ArrayList<Empleado>)query.getResultList();
        return lista;
    }


    public void modificarEmpleado(Empleado empleadoSeleccionado) {
        sesion.beginTransaction();
        sesion.saveOrUpdate(empleadoSeleccionado);
        sesion.getTransaction().commit();

    }


    public void borrarEmpleado(Empleado empleadoBorrado) {
        sesion.beginTransaction();
        sesion.delete(empleadoBorrado);
        sesion.getTransaction().commit();
    }

    /**
     * Permite recolecta los socios que tiene X instructor
     * @param empleadoLocal el instructor a ver
     * @return Lista de Socios de X instructor
     */
    public ArrayList<Empleado> getEmpleadoLocal(Local empleadoLocal) {
        Query query = sesion.createQuery("FROM Empleado WHERE local= :prop");
        query.setParameter("prop", empleadoLocal);
        ArrayList<Empleado> lista = (ArrayList<Empleado>) query.getResultList();
        return lista;
    }


    public void altaPedido(Pedido nuevoPedido) {
        System.out.println(nuevoPedido);
        sesion.beginTransaction();
        sesion.save(nuevoPedido);
        sesion.getTransaction().commit();

    }


    public ArrayList<Pedido> getPedido() {
        Query query = sesion.createQuery("FROM Pedido");
        ArrayList<Pedido> lista = (ArrayList<Pedido>)query.getResultList();
        return lista;
    }


    public void modificarPedido(Pedido pedidoSeleccionado) {
        sesion.beginTransaction();
        sesion.saveOrUpdate(pedidoSeleccionado);
        sesion.getTransaction().commit();
    }


    public void borrarPedido(Pedido pedidoBorrado) {
        sesion.beginTransaction();
        sesion.delete(pedidoBorrado);
        sesion.getTransaction().commit();
    }


    public void altaProducto(Producto nuevoProducto) {
        sesion.beginTransaction();
        sesion.save(nuevoProducto);
        sesion.getTransaction().commit();

    }


    public ArrayList<Producto> getProducto() {
        Query query = sesion.createQuery("FROM Producto");
        ArrayList<Producto> lista = (ArrayList<Producto>)query.getResultList();
        return lista;
    }


    public void modificarProducto(Producto productoSeleccionado) {
        sesion.beginTransaction();
        sesion.saveOrUpdate(productoSeleccionado);
        sesion.getTransaction().commit();
    }


    public void borrarProducto(Producto productoBorrado) {
        sesion.beginTransaction();
        sesion.delete(productoBorrado);
        sesion.getTransaction().commit();
    }






}
